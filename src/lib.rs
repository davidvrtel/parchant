use clap::arg_enum;
use std::{fs, path::PathBuf};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub struct Cli {
  /// What to parse.
  #[structopt(possible_values = &Type::variants(), case_insensitive = true)]
  parse_type: Type,
  /// Path to changelog markdown file.
  #[structopt(parse(try_from_str = parse_path), default_value = "CHANGELOG.md")]
  path: PathBuf,
  /// Pattern to search for.
  #[structopt(default_value = "## v")]
  pattern: String,
}

arg_enum! {
  #[derive(Debug, PartialEq)]
  enum Type {
    Tag,
    Desc,
  }
}

fn parse_path(src: &str) -> Result<PathBuf, String> {
  let path = PathBuf::from(src);
  match path.exists() {
    true => Ok(path),
    false => Err(String::from(format!("File {} doesn't exist!", src))),
  }
}

pub fn run(config: Cli) -> Result<(), &'static str> {
  let contents = match fs::read_to_string(config.path) {
    Ok(c) => c,
    Err(_) => return Err("Can't read the file contents. Does the file exist?"),
  };

  let results = match &config.parse_type {
    Type::Tag => parse_tags(&contents, &config.pattern),
    Type::Desc => parse_desc(&contents, &config.pattern),
  };

  for line in results {
    println!("{}", line);
  }

  Ok(())
}

fn parse_tags<'a>(contents: &'a str, pattern: &str) -> Vec<&'a str> {
  let mut result = Vec::new();

  for line in contents.lines() {
    if line.starts_with(pattern) {
      result.push(line.split_whitespace().nth(1).unwrap());
      break;
    }
  }
  result
}

fn parse_desc<'a>(contents: &'a str, pattern: &str) -> Vec<&'a str> {
  let mut result = Vec::new();
  let mut parsing = false;

  for line in contents.lines() {
    match parsing {
      false => {
        if line.starts_with(pattern) {
          parsing = true;
          result.push(line);
        }
      }
      true => {
        if line.starts_with(pattern) {
          break;
        } else {
          result.push(line);
        }
      }
    }
  }
  result
}
